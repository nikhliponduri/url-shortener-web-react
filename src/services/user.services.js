import { makeRequest, GET, POST } from "../utils/fetch";

export const postUserLogin = (body) => {
    return makeRequest(POST, '/user/login', body);
}

export const getUserDetails = (body) => {
    return makeRequest(GET, '/user/ping');
}

export const postUserRegister = (body) => {
    return makeRequest(POST, `/user/register`, body);
}

export const postUrlService = (body) => {
    return makeRequest(POST, '/url', body);
}

export const getUrlService = () => {
    return makeRequest(GET, '/url')
}