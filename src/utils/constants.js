export const NETWORK_ERROR = "Please check your network connection";
export const FAILURE = "FAILURE";
export const SUCCESS = "SUCCESS";
export const SITE_URL = 'https://urmi.herokuapp.com'