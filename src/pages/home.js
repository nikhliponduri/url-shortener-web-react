import React, { useState } from 'react'
import { isValidUrl } from '../validation/validations';
import { resolveByType } from '../utils/utils';
import { toast } from 'react-toastify';
import FormInput from '../components/shared/form-input';
import Button from '../components/shared/button';
import { SITE_URL } from '../utils/constants';

const Home = ({ postUrlAction }) => {
    const [url, setUrl] = useState('');
    const [loading, setLoading] = useState(false);
    const [formErrors, setFormErrors] = useState({});
    const serverCallback = ({ type, message, data }) => {
        setLoading(false)
        resolveByType({
            type,
            success: () => {
                navigator.clipboard.writeText(`${SITE_URL}/re/${data}`).then(() => {
                    toast.success('Link copied to clipboard');
                })
            },
            failure: () => {
                toast.error(message);
            }
        })
    }
    const submit = (e) => {
        e.preventDefault();
        const validation = isValidUrl({ url });
        if (!validation.isValidForm) {
            setFormErrors(validation.errors);
        }
        else {
            setLoading(true);
            setFormErrors({})
            postUrlAction({ url }, serverCallback);
        }

    }
    return (
        <div className='home'>
            <div className='banner'>
                <img alt='' src='https://i1.wp.com/www.dignited.com/wp-content/uploads/2019/08/Best-URL-Shortening-Tools-For-2018-1024x768.jpg?fit=640%2C480&ssl=1' />
            </div>
            <div className='black'>
            </div>
            <div className="row">
                <div className="col-lg-7 col-md-8 col-sm-18 p-0 text-center">
                    <FormInput error={formErrors.url} value={url} onChange={(e) => setUrl(e.target.value)} type="text" className="form-control search-slt" placeholder="Enter a Url" />
                </div>
                <div className="col-lg-3 col-md-3 col-sm-12 p-0">
                    <Button title='generate' loading={loading} onClick={submit} disabled={loading} type="button" className="btn btn-danger wrn-btn" />
                </div>
            </div>
        </div>
    )
}

export default Home
