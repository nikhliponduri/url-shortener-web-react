import axios from 'axios';
import { postUserLogin, getUserDetails, postUserRegister, postUrlService, getUrlService } from '../services/user.services';
import { sendCallback, checkError } from '../utils/utils';
import { setUserProfile, loginSucess, setSessionLoading, logoutSucess } from '../redux/session.reducer';
import { setUserAllUrls } from '../redux/user.reducer';


export const userLoginAction = (info, callback) => async (dispatch, getState) => {
    try {
        const res = await postUserLogin(info);
        localStorage.setItem('authtoken', `Bearer ${res.data.data}`);
        await dispatch(userDetailsAction());
        sendCallback(callback);
    } catch (error) {
        checkError(error, callback);
    }
}

export const userDetailsAction = (callback) => async (dispatch, getState) => {
    try {
        console.log(localStorage.getItem('authtoken'))
        axios.defaults.headers['Authorization'] = await localStorage.getItem('authtoken');
        const res = await getUserDetails();
        dispatch(setUserProfile(res.data));
        dispatch(loginSucess());
        dispatch(setSessionLoading(false))
        sendCallback(callback);
    } catch (error) {
        checkError(error, callback);
        dispatch(setSessionLoading(false))
    }
}

export const userRegisterAction = (info, callback) => async (dispatch, getState) => {
    try {
        await postUserRegister(info);
        sendCallback(callback);
    } catch (error) {
        checkError(error, callback);
    }
}

export const userLogoutAction = (callback) => async (dispatch, getSatate) => {
    await dispatch(logoutSucess())
    delete axios.defaults.headers.common["Authorization"];
    localStorage.removeItem('authtoken');
    dispatch(setSessionLoading(false));
    sendCallback(callback);
}

export const postUrlAction = (info, callback) => async (dispatch, getState) => {
    try {
        const res = await postUrlService(info);
        sendCallback(callback, res.data.data);
    } catch (error) {
        checkError(error, callback)
    }
}

export const userUrlsAction = (callback) => async (dispatch, getSatate) => {
    try {
        dispatch(setUserAllUrls(undefined));
        const res = await getUrlService();
        dispatch(setUserAllUrls(res.data.data));
    } catch (error) {
        checkError(error, callback);
        dispatch(setUserAllUrls(null));

    }
}