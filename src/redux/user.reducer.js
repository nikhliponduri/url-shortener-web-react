const intialState = {
    allUrls: undefined
}

const SET_USER_All_URLS = 'SET_USER_All_URLS';
const LOGIN_SUCESS = 'LOGGIN_SUCCESS';
const LOGOUT_SUCESS = 'LOGOUT_SUCESS';


export const setUserAllUrls = (allUrls) => ({
    type: SET_USER_All_URLS,
    allUrls
});


const userReducer = (user = intialState, action) => {
    switch (action.type) {
        case LOGIN_SUCESS:
            return {
                ...user,
                loggedIn: true
            }
        case LOGOUT_SUCESS:
            return {
                ...intialState
            }
        case SET_USER_All_URLS:
            return {
                ...user,
                allUrls: action.allUrls
            }
        default:
            return user
    }
}

export default userReducer;