import validator, { EMAIL, PASSWORD } from "./validator";

export const isValidRegisterForm = ({ email = '', password = '' }) => {
    const options = [
        { type: EMAIL, value: email, key: 'email', min: 1 },
        { type: PASSWORD, value: password, min: 1, max: 20, key: 'password' },
    ]
    return validator(options);
}

export const isValidLoginForm = ({ email = '', password = '' }) => {
    const options = [
        { type: EMAIL, value: email, key: 'email', min: 1 },
        { type: PASSWORD, value: password, min: 1, max: 20, key: 'password' },
    ]
    return validator(options);
}

export const isValidUrl = ({ url = '' }) => {
    const options = [{ type: URL, key: 'url', min: 1, value: url }]
    return validator(options)
}