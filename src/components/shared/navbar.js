import React, { Fragment } from 'react';
import { NavLink, withRouter } from 'react-router-dom'

const Navbar = ({ loggedIn, history: { push }, userLogoutAction }) => {
    return (
        <nav className="navbar navbar-expand-lg navbar-light" >
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <NavLink className="navbar-brand" to='/'><h4>Url Shortener</h4></NavLink>

            <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
                <ul className="navbar-nav ml-auto mt-2 mt-lg-0 text-center">
                    {!loggedIn ?
                        <Fragment>
                            <li className="nav-item">
                                <NavLink activeClassName='active-class' className="nav-link text-white" to='/login'>Login</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink activeClassName='active-class' className="nav-link text-white" to='/register'>Register</NavLink>
                            </li>
                        </Fragment> :
                        <Fragment>
                            <li className="nav-item">
                                <NavLink activeClassName='active-class' className="nav-link text-white" to='/my-urls'>my-urls</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink activeClassName='active-class' to='/login' className="nav-link text-white" onClick={(e) => {
                                    userLogoutAction()
                                }}>Logout</NavLink>
                            </li>
                        </Fragment>
                    }
                </ul>
            </div>
        </nav>
    )
}

export default withRouter(Navbar)
