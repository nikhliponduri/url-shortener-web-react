import React, { lazy, Suspense } from 'react';
import { Switch, Route } from 'react-router-dom';
import AuthRoute from './shared/auth-route';
import AppLoader from './shared/app-loader';
import myUrls from '../containers/my-urls';

const login = lazy(() => import('../containers/login'));
const register = lazy(() => import('../containers/register'));
const Home = lazy(() => import('../containers/home'));


const AppRouter = ({ loggedIn }) => {
    return (
        <Suspense fallback={<AppLoader loading={true} />}>
            <Switch>
                <Route path='/' exact={true} component={Home} />
                <AuthRoute path='/login' exact={true} loggedIn={!loggedIn} isValidRole={!loggedIn} component={login} />
                <AuthRoute path='/register' exact={true} loggedIn={!loggedIn} isValidRole={!loggedIn} component={register} />
                <AuthRoute path='/my-urls' exact={true} loggedIn={loggedIn} isValidRole={loggedIn} component={myUrls} />
            </Switch>
        </Suspense>
    )
}

export default AppRouter
