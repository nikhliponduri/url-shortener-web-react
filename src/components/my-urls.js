import React, { Fragment, useEffect } from 'react';
import { checkLoading } from '../utils/utils'
import AppLoader from './shared/app-loader'
import { SITE_URL } from '../utils/constants';

const MyUrls = (props) => {
    const { allUrls, userUrlsAction } = props;
    useEffect(() => {
        userUrlsAction();
    }, [userUrlsAction]);
    return (
        <div className='container'>
            <AppLoader loading={checkLoading(allUrls)} failType='Coudnt fetch now'>
                {
                    allUrls &&
                    <div className='container' style={{ marginTop: '20px' }}>
                        {
                            Array.isArray(allUrls) && !allUrls[0] ?
                                <div className='display-center text-center'>
                                    <span className='text-center'>No Generated Urls to display</span>
                                </div>
                                :
                                <Fragment>
                                    <table class="table">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Original Url</th>
                                                <th scope='col'>Shortened Url</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {allUrls.map((url, i) => (
                                                <tr>
                                                    <th scope="row">{i + 1}</th>
                                                    <td><a href={url.originalUrl.substr(0, 4) === 'http' ? url.originalUrl : `https://${url.originalUrl}`} target='_blank' rel="noopener noreferrer">{url.originalUrl}</a></td>
                                                    <td><a href={`${SITE_URL}/re/${url.shortenedUrl}`} target='_blank' rel="noopener noreferrer">{SITE_URL}/re/{url.shortenedUrl}</a></td>
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                </Fragment>
                        }
                    </div>
                }
            </AppLoader>
        </div>
    )
}

export default MyUrls
