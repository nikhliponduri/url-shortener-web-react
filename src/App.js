import React from 'react';
import Navbar from './containers/navbar';
import { ToastContainer } from 'react-toastify';
import AppRouter from './containers/app-router';
import 'react-toastify/dist/ReactToastify.css';
import AppLoader from './components/shared/app-loader';


function App({ loading }) {
  return (
    <div className='App'>
      <ToastContainer />
      <AppLoader loading={loading}>
        <Navbar />
        <AppRouter />
      </AppLoader>
    </div>
  );
}

export default App;
