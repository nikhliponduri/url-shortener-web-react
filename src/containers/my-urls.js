import { connect } from "react-redux";
import MyUrls from "../components/my-urls";
import { userUrlsAction } from "../actions/user.actions";

const mapStateToProps = (state) => ({
    allUrls: state.user.allUrls
});

export default connect(mapStateToProps, { userUrlsAction })(MyUrls)