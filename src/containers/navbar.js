import { connect } from "react-redux";
import navbar from "../components/shared/navbar";
import { userLogoutAction } from "../actions/user.actions";

const mapStateToProps = (state) => ({
    loggedIn: state.session.loggedIn
});

export default connect(mapStateToProps,{userLogoutAction})(navbar)