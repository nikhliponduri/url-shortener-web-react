import { connect } from 'react-redux';
import { userLoginAction } from "../actions/user.actions";
import login from "../pages/login";

const mapDispatchToProps = (dispatch) => ({
    userLoginAction: (info, callback) => dispatch(userLoginAction(info, callback))
});

export default connect(null, mapDispatchToProps)(login)