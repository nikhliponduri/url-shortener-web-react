import { connect } from 'react-redux';
import AppRouter from '../components/app-router';

const mapStateToProps = (state) => ({
    loggedIn: state.session.loggedIn
});

export default connect(mapStateToProps)(AppRouter);