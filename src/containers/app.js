import { connect } from "react-redux";
import App from "../App";

const mapStateToProps = (state) => ({
    loading: state.session.loading
});

export default connect(mapStateToProps)(App)