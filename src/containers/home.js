import { connect } from 'react-redux';
import { postUrlAction } from '../actions/user.actions';
import Home from '../pages/home';

const mapDispatchToProps = (dispatch) => ({
    postUrlAction: (info, callback) => dispatch(postUrlAction(info, callback))
});

export default connect(null, mapDispatchToProps)(Home)