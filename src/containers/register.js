import { connect } from 'react-redux';
import { userRegisterAction } from "../actions/user.actions";
import register from '../pages/register';

const mapDispatchToProps = (dispatch) => ({
    userRegisterAction: (info, callback) => dispatch(userRegisterAction(info, callback))
});

export default connect(null, mapDispatchToProps)(register)